// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <CoreMinimal.h>
#include <Kismet/BlueprintFunctionLibrary.h>
#include <Engine/DataTable.h>
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Run_state UMETA(DisplayName = "Run State"),
    AimRun_state UMETA(DisplayName = "Aim Run State"),
    Walk_state UMETA(DisplayName = "Walk Run State"),
    AimWalk_state UMETA(DisplayName = "AimWalk Run State"),
    Sneak_state UMETA(DisplayName = "Sneak State"),
    AimSneak_state UMETA(DisplayName = "Aim Sneak Run State"),
    Aim_state UMETA(DisplayName = "Aim State"),
    Idle_state UMETA(DisplayName = "Idle Run State")
};


USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Run_Speed = 700.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimRun_Speed = 500.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Walk_Speed = 400.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimWalk_Speed = 200.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Sneak_Speed = 100.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSneak_Speed = 50.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Aim_Speed = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float Idle_Speed = 0.0f;
};


USTRUCT(BlueprintType)
struct FProjectileInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    TSubclassOf<class AProjectileDefault> Projectile = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileDamage = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileLifeTime = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileInitSpeed = 2000.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    USoundBase* HitSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    UParticleSystem* ExloseFX = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    USoundBase* ExploseSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ProjectileMaxRadiusDamage = 200.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
    float ExploseMaxDamage = 40.0f;

};


USTRUCT(BlueprintType)
struct FWeaponDispersion
{

    GENERATED_BODY()
    /*Run*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDespersionMax = 10.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDespersionMin = 4.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Run_StateDispersionReduction = 0.1f;
    /*AimRun*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimRun_StateDespersionMax = 5.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimRun_StateDespersionMin = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimRun_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimRun_StateDispersionReduction = 0.1f;
    /*Walk*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDespersionMax = 8.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDespersionMin = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Walk_StateDispersionReduction = 0.2f;
    /*AimWalk*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDespersionMax = 4.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDespersionMin = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimWalk_StateDispersionReduction = 0.2f;
    /*Sneak*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Sneak_StateDespersionMax = 4.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Sneak_StateDespersionMin = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Sneak_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Sneak_StateDispersionReduction = 0.4f;
    /*AimSneak*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimSneak_StateDespersionMax = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimSneak_StateDespersionMin = 0.2f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimSneak_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float AimSneak_StateDispersionReduction = 0.4f;
    /*Aim*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDespersionMax = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDespersionMin = 0.1f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Aim_StateDispersionReduction = 0.5f;
    /*Idle*/
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Idle_StateDespersionMax = 3.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Idle_StateDespersionMin = 0.3f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Idle_StateDespersionRecoil = 1.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
        float Idle_StateDispersionReduction = 0.4f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
    GENERATED_BODY()
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
    TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
    float RateOfFire = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
    float ReloadTime = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
    int32 MaxRound = 10;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
    int32 NumberProjectileByShot = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    FWeaponDispersion Dispersion;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundBase* SoundFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
    USoundBase* SoundReload = nullptr;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    UParticleSystem* EffectFire = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    FProjectileInfo ProjectileSetting;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
    float Damage = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
    float DistanceTrace = 2000.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
    UDecalComponent* DecalOnHit = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
    UAnimMontage* AnimCharReload = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    UStaticMesh* MagazineDrop = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    UStaticMesh* BulletShell = nullptr;

};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
    int32 round = 30;
};


UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};