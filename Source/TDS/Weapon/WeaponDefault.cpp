// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
		
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetRound() > 0) {
		if (WeaponFiring) {
			if (FireTimer < 0.f) {
				if (!WeaponReloading)
					Fire();
			}
			else {
				FireTimer -= DeltaTime;
			}
		}
	}
	else {
		if (!WeaponReloading)
			InitReloading();
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading) {
		if (ReloadTimer < 0.0f) {
			FinishReload();
		}
		else {
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	AdditionalWeaponInfo.round = WeaponInfo.MaxRound;
	ReloadTime = WeaponInfo.ReloadTime; // remove
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return true;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponInfo.ProjectileSetting;
}

void AWeaponDefault::Fire()
{	
	//UE_LOG(LogTemp, Warning, TEXT(" AWeaponDefault::Fire"));

	FireTimer = WeaponInfo.RateOfFire;
	AdditionalWeaponInfo.round--;

	if (ShootLocation) {
	
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRatation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		
		
		if (ProjectileInfo.Projectile) {
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();

			AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRatation, SpawnParams));
			if (myProjectile) {
				myProjectile->InitProjectile(ProjectileInfo);
				//UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::Fire  ProjectileInfo initspeed = %f"), ProjectileInfo.ProjectileInitSpeed);
				myProjectile->InitialLifeSpan = 20.f;
			}


		}
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
}

void AWeaponDefault::ChangeDispersion()
{
}

int32 AWeaponDefault::GetRound()
{
	return AdditionalWeaponInfo.round;
}

void AWeaponDefault::InitReloading()
{
	WeaponReloading = true;
	ReloadTimer = WeaponInfo.ReloadTime;

	//ToDo add Anim
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	AdditionalWeaponInfo.round = WeaponInfo.MaxRound;
}

