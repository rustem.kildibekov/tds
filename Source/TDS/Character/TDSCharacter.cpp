// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "../Game/TDSGameInstance.h"


ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	
	
	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && !tempStop)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* _InputComponent)
{
	Super::SetupPlayerInputComponent(_InputComponent);
	
	_InputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	_InputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);
	_InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	_InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	_InputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputReloadWeapon);

}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{

	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDSCharacter::InputReloadWeapon()
{
	if (CurrentWeapon) {
		if (CurrentWeapon->GetRound() < CurrentWeapon->WeaponInfo.MaxRound)
			CurrentWeapon->InitReloading();
	}
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	//UE_LOG(LogTemp, Warning, TEXT("AxisX %f,  AxisY  %f") , AxisX, AxisY);
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	FRotator direction = FVector(AxisX, AxisY, 0.0f).ToOrientationRotator();
	if (abs(abs(direction.Yaw) - abs(this->GetActorRotation().Yaw)) < 30)
		forwardMoving = true;
	else 
		forwardMoving = false;
	

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController) {
		FHitResult resultHit;
		myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, resultHit);
		FRotator newRotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), resultHit.Location);
		SetActorRotation(FQuat(FRotator(0.0f, newRotator.Yaw, 0.0f)));

		
	}
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{


	AWeaponDefault* myWeapon = GetCurrentWeapon();

	if (myWeapon) {
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::AttackCharEvent - no myWeapon"))
	}

}

void ATDSCharacter::characterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Run_state:
		ResSpeed = MovementInfo.Run_Speed;
		break;
	case EMovementState::AimRun_state:
		ResSpeed = MovementInfo.AimRun_Speed;
		break;
	case EMovementState::Walk_state:
	ResSpeed = MovementInfo.Walk_Speed;
		break;
	case EMovementState::AimWalk_state:
		ResSpeed = MovementInfo.AimWalk_Speed;
		break;
	case EMovementState::Sneak_state:
		ResSpeed = MovementInfo.Sneak_Speed;
		break;
	case EMovementState::AimSneak_state:
		ResSpeed = MovementInfo.AimSneak_Speed;
		break;
	default:
		ResSpeed = MovementInfo.Walk_Speed;
		break;
	}
	if (!forwardMoving) ResSpeed *= 0.9;
		
	//UE_LOG(LogTemp, Warning, TEXT("Speed %f"), ResSpeed);
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (AxisX == 0.f && AxisY == 0.f) {  //WASD pressed
		if (AimEnabled) MovementState = EMovementState::Aim_state;
		else MovementState = EMovementState::Idle_state;
	}
	else {
		if (AimEnabled && RunEnabled && SneakEnabled) MovementState = EMovementState::AimSneak_state;
		else if (AimEnabled && RunEnabled && !SneakEnabled) MovementState = EMovementState::AimRun_state;
		else if (AimEnabled && !RunEnabled && SneakEnabled) MovementState = EMovementState::AimSneak_state;
		else if (!AimEnabled && RunEnabled && SneakEnabled) MovementState = EMovementState::Sneak_state;
		else if (AimEnabled && !RunEnabled && !SneakEnabled) MovementState = EMovementState::AimWalk_state;
		else if (!AimEnabled && RunEnabled && !SneakEnabled) MovementState = EMovementState::Run_state;
		else if (!AimEnabled && !RunEnabled && SneakEnabled) MovementState = EMovementState::Sneak_state;
		else if (!AimEnabled && !RunEnabled && !SneakEnabled) MovementState = EMovementState::Walk_state;
	}


	characterUpdate();
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}



void ATDSCharacter::InitWeapon(FName idWeapon)
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI) {
		if (myGI->GetWeaponInfoByName(idWeapon, myWeaponInfo)) {
			if (myWeaponInfo.WeaponClass) {
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon) {
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					CurrentWeapon->WeaponInfo = myWeaponInfo;
					CurrentWeapon->UpdateStateWeapon(MovementState);
				}
			}
		
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - %s not found in table"), *idWeapon.ToString());
		}

	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - UTDSGameInstance not found"));
	}
	
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}
