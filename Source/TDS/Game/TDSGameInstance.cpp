// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo)
{
    bool bIsFind = false;
    FWeaponInfo* WeaponInfoRow;

    if (WeaponInfoTable) {
        WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName,"", false);
        if (WeaponInfoRow) {
            bIsFind = true;
            OutInfo = *WeaponInfoRow;
        }
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName -  WeaponInfoTable is Null"));
    }
    return bIsFind;
}
